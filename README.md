# [Goal Tracker]

## Introduction
This app aims to help user to create and track there goals. Some of the features of the app includes.
- Login
- SignUp
- Create your goals.
- Edit your Goals.
- Delete your goals.
- Add Task to your goals.
- Edit your task.
- Delete your task.
- Mark your task as complete.


## Architecture
![Architecture](./Architecture.jpg)

## Database ERD
![Architecture](./Final%20ERD.jpg)

## API ENDPOINTS

| No. | Endpoint                               | Request Type | Purpose           |
|-----|----------------------------------------|--------------|-------------------|
| 1   | /api/v1/users/login                   | POST         | Login user        |
| 2   | /api/v1/users/signup                  | POST         | Signup user       |
| 3   | /api/v1/goals                         | POST         | Create goal       |
| 4   | /api/v1/goals                         | GET          | Get all goals     |
| 5   | /api/v1/goals/:goalId                 | PATCH        | Update goal       |
| 6   | /api/v1/goals/:goalId                 | DELETE       | Delete goal       |
| 7   | /api/v1/goals/:goalId                 | GET          | Get a goal        |
| 8   | /api/v1/goals/:goalId/tasks           | POST         | Create Task       |
| 9   | /api/v1/tasks/:taskId                 | PATCH        | Update Task       |
| 10  | /api/v1/tasks/:taskId                 | DELETE       | Delete Task       |
| 11  | /api/v1/tasks/:taskId                 | GET          | Get a Task        |
| 12  | /api/v1/goals/:goalId/tasks/:taskId  | PATCH        | Mark as Complete  |


## Technologies Used
- Frontend
    - React
    - MUI 
    - Joi
    - React Router Dom
    - Axios

- Backend
    - Node
    - Express 
    - Mongoose
    - Joi

- Database
    - MongoDB

- Error Tracking Frontend
    - Sentry

## Setup Instructions
Please follow the step-by-step instructions on how to set up the development environment and run the web app locally.

### Frontend Setup:
1. Navigate to the `frontend-goal-tracker` directory.
2. Run `npm install` to install dependencies.
3. Run `npm start` to start the app on port 3000
   
### Backend Setup:
1. Navigate to the `backend-goal-tracker` directory.
2. Run `npm install` to install dependencies.
3. Run `npm start` to start the server locally.

## Testing
- Currently the application was manually tested.
- Future Scope : (For both Frontend and Backend)
    - Unit Test
    - Integration Test 
    - E2E Test

## Deployment
- The Frontend is currently deployed on Netlify.
- The Backend is currently deployed on AWS EC2.
- The Database is currently deployed on MongoDB.

- Future Scope :
    - Setting up CI/CD pipeline with various stages e.g: linting, build, Git tag creation, and deployment(dev and prod env)

## Monitoring and Maintenance

- Frontend 
    - Sentry : For tracking bugs and measuring performance.

- Backend
    - Winston for logging errors.

## Security Considerations
Following are the security measures inplemented in this app.
- CORS to prevent Cross Origin Resource Sharing
- Rate Limiter for preventing bruteforce and DDOS attacks.
- Mongo Sanitize to prevent NoSQL injection attacks
- XSS to prevent Cross Site Scripting