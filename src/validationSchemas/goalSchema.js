const Joi = require("joi");

export const goalSchema = Joi.object({
  goalName: Joi.string().trim().min(6).required(),
  minTimeline: Joi.date().required().min(new Date(new Date().setHours(0, 0, 0, 0)).toISOString(), "today").messages({
    "date.min": "Min timeline must be greater than today",
  }),
  maxTimeline: Joi.date().required().min(Joi.ref("minTimeline")).messages({
    "date.min": "Max timeline must be greater than or equal to min timeline",
  }),
});
