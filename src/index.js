import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import DataProvider from './context/DataContext'
import * as Sentry from "@sentry/react";
import { ErrorBoundary } from 'react-error-boundary';
import SomethingWentWrong from './components/SomethingWentWrong';

Sentry.init({
  dsn: "https://0ac2c94ca33a4c90820617f1ab505d81@o4507091593920512.ingest.us.sentry.io/4507091653033984",
  integrations: [
    Sentry.browserTracingIntegration(),
    Sentry.replayIntegration(),
  ],
  tracesSampleRate: 1.0, 
  tracePropagationTargets: ["localhost", /^https:\/\/yourserver\.io\/api/],
  replaysSessionSampleRate: 0.1,
  replaysOnErrorSampleRate: 1.0, 
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <>
    <DataProvider>
      <ErrorBoundary FallbackComponent={<SomethingWentWrong />}>
        <App />
      </ErrorBoundary>
    </DataProvider>
  </>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
