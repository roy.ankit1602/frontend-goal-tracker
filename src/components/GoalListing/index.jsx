import React, { useEffect, useState } from "react";
import {
  Paper,
  Typography,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Stack,
  Tooltip,
  Chip,
  Box,
} from "@mui/material";
import { Add, Delete, Done, Edit } from "@mui/icons-material";
import { Pagination } from "@mui/material";
import { useDataContext } from "../../context/DataContext";
import { GoalService } from "../../services/goal.services";
import { displayError } from "../../utils";
import { useCookies } from "react-cookie";
import Task from "../Task";
import { TaskService } from "../../services/task.services";
import { toast } from "react-toastify";

const GoalListing = ({
  addTask,
  onGoalClick,
  onTaskClick,
  onAddGoal,
  onDeleteGoal,
  onDeleteTask,
}) => {
  const {
    goals,
    setCurrentTaskId,
    setOpenGoalModal,
    setOpenTaskModal,
    openTaskModal,
    setIsTaskEdit,
    setIsGoalEdit,
    setGoals,
    setCurrentTaskGoalId,
    showLoader,
    setShowLoader,
    setCurrentGoalForSuggestion,
  } = useDataContext();
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [cookies] = useCookies(["token"]);

  const handleGoalClick = (goalId) => {
    if (onGoalClick) {
      onGoalClick(goalId);
    }
  };

  const markCompletedTask = async (goalId, taskId) => {
    try {
      setShowLoader(true);
      await TaskService.markTaskAsComplete(goalId,taskId,cookies.token);
      setShowLoader(false);
      toast.success("Task Marked as done");

      const {
        data: {
          data: { goals },
        },
      } = await GoalService.getGoals(cookies.token);
      setGoals(goals);
    } catch (error) {
      displayError(error);
      setShowLoader(false);
    }
  }

  const handleTaskClick = (goalId, taskId) => {
    if (onTaskClick) {
      onTaskClick(goalId, taskId);
    }
  };

  const handleAddGoal = () => {
    setIsGoalEdit(false);
    if (onAddGoal) {
      onAddGoal();
    }
  };

  const handleUpdateGoal = (goalId) => {
    setCurrentTaskGoalId(goalId);
    setCurrentGoalForSuggestion(null);
    setIsGoalEdit(true);
    setOpenGoalModal(true);
  };

  const handleDeleteGoal = (goalId) => {
    if (onDeleteGoal) {
      onDeleteGoal(goalId);
    }
  };

  const handleUpdateTask = (goalId, taskId) => {
    setOpenTaskModal(true);
    setCurrentTaskId(taskId);
    setCurrentTaskGoalId(goalId);
    setIsTaskEdit(true);
  };

  const handleDeleteTask = (taskId) => {
    if (onDeleteTask) {
      onDeleteTask(taskId);
    }
  };

  const handleAddTask = (goalId) => {
    setCurrentTaskGoalId(goalId);
    if (addTask) {
      addTask(goalId);
    }
  };
  let pageCount;
  useEffect(() => {
    const fetchGoals = async () => {
      try {
        const {
          data: {
            data: { goals, totalPages },
          },
        } = await GoalService.getGoals(cookies.token, page);
        setTotalPages(totalPages);
        setGoals(goals);
      } catch (error) {
        console.error(error);
        displayError(error);
      }
    };

    fetchGoals();
  }, [page]);

  return (
    <>
      {openTaskModal && <Task />}
      <Paper elevation={3} style={{ padding: 20 }}>
        <Stack
          display="flex"
          flexDirection="row"
          justifyContent="space-between"
          marginBottom={"20px"}
        >
          <Typography variant="h3"> My Goals</Typography>
          <Tooltip title="Add Goal">
            <IconButton onClick={handleAddGoal} aria-label="add-goal">
              <Add />
            </IconButton>
          </Tooltip>
        </Stack>
        {goals?.map((goal, index) => (
          <div key={index} style={{ marginBottom: 20 }}>
            <Stack
              display="flex"
              flexDirection="row"
              justifyContent="space-between"
            >
              <Typography
                onClick={() => handleGoalClick(goal._id)}
                variant="h5"
                color="primary"
              >
                {goal.goalName}
              </Typography>
              <Stack display="flex" flexDirection="row">
                <Tooltip title="Add Task">
                  <IconButton
                    onClick={() => handleAddTask(goal._id)}
                    aria-label="add-task"
                  >
                    <Add />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Edit Goal">
                  <IconButton
                    onClick={() => handleUpdateGoal(goal._id)}
                    aria-label="edit-goal"
                  >
                    <Edit />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Delete Goal">
                  <IconButton
                    onClick={() => handleDeleteGoal(goal._id)}
                    aria-label="delete-goal"
                  >
                    <Delete />
                  </IconButton>
                </Tooltip>
              </Stack>
            </Stack>
            <List>
              {goal.tasks.map((task, taskIndex) => (
                <ListItem
                  key={taskIndex}
                  onClick={() => handleTaskClick(goal._id, task._id)}
                >
                  <ListItem
                    sx={{
                      "&:hover": {
                        backgroundColor: "#fafafa", // Change to desired hover color
                        cursor: "pointer",
                      },
                      width: "90%",
                    }}
                  >
                    <ListItemText primary={task.taskName} />
                    <Chip
                      label={`${task.logs.length}/${task.frequency}`}
                      color={
                        task.logs.length == task.frequency
                          ? "success"
                          : "primary"
                      }
                    />
                  </ListItem>
                  <ListItemSecondaryAction>
                    <Tooltip title="Mark Task as complete">
                      <IconButton
                        onClick={() => markCompletedTask(goal._id, task._id)}
                        edge="end"
                        aria-label="mark-completed-task"
                        disabled={task.logs.length === task.frequency}
                      >
                        <Done
                          color={
                            task.logs.length === task.frequency
                              ? "success"
                              : "primary"
                          }
                        />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Delete Task">
                      <IconButton
                        onClick={() => handleUpdateTask(goal._id, task._id)}
                        edge="end"
                        aria-label="edit-task"
                      >
                        <Edit />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Edit this Task">
                      <IconButton
                        onClick={() => handleDeleteTask(task._id)}
                        edge="end"
                        aria-label="delete-task"
                      >
                        <Delete />
                      </IconButton>
                    </Tooltip>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>
          </div>
        ))}
        {goals.length > 0 && (
          <Pagination
            count={pageCount}
            page={page}
            onChange={(e, value) => setPage(value)}
            variant="outlined"
            shape="rounded"
          />
        )}
      </Paper>
    </>
  );
};

export default GoalListing;
