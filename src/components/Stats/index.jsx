import React from 'react';
import {
  Paper,
  Typography,
  Grid,
  List,
  ListItem,
  ListItemText,
} from "@mui/material";
import { useDataContext } from '../../context/DataContext';
import moment from "moment";

const Stats = () => {
  const { task } = useDataContext()
  const formattedTime = moment(task.reminderTime).format("hh:mm:ss A");
  return (
    <Paper elevation={3} style={{ padding: 20 }}>
      <Grid justifyContent="center" container spacing={2}>
        <Grid
          item
          xs={8}
          style={{
            border: "1px solid #e0e0e0",
            marginTop: "5%",
            padding: "5%",
          }}
        >
          <Typography variant="h6" color="primary">
            Frequency
          </Typography>
          <Typography variant="h4">{task.frequency}</Typography>
        </Grid>
        <Grid
          item
          xs={8}
          style={{
            border: "1px solid #e0e0e0",
            marginTop: "5%",
            padding: "5%",
          }}
        >
          <Typography variant="h6" color="primary">
            Quantity
          </Typography>
          <Typography variant="h5">{task.quantity}</Typography>
        </Grid>
        <Grid
          item
          xs={8}
          style={{
            border: "1px solid #e0e0e0",
            marginTop: "5%",
            padding: "5%",
          }}
        >
          <Typography variant="h6" color="primary">
            Reminder Time
          </Typography>
          <Typography variant="h5">{formattedTime}</Typography>
        </Grid>
        <List>
          <Typography variant="h5">Logs</Typography>
          {task.logs.map((log, index) => (
            <ListItem
              key={index}
              sx={{
                "&:hover": {
                  backgroundColor: "#fafafa", // Change to desired hover color
                  cursor: "pointer",
                },
              }}
            >
              <Typography color="green" variant="body1">
                Completed at: {moment(log.timestamp).format("D MMM YYYY h:mmA")}
              </Typography>
            </ListItem>
          ))}
        </List>
      </Grid>
    </Paper>
  );
};

export default Stats;
