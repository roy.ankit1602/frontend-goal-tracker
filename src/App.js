import React, { useEffect } from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Home from "./components/Home";
import Navbar from "./components/Navbar";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useDataContext } from "./context/DataContext";
import { useCookies } from "react-cookie";
import { GoalService } from "./services/goal.services";
import NotFound from "./components/NotFound";
import { displayError } from "./utils";
import { RotatingTriangles } from "react-loader-spinner";

const Backdrop = ({ show }) => {
  return show ? <div className="backdrop"></div> : null;
};

function App() {
  const [cookies] = useCookies(["token", "user"]);
  const { showLoader, setShowLoader, setUser, setGoals, setTask, setGoal } =
    useDataContext();

  const fetchGoals = async () => {
    try {
      setShowLoader(true)
      const {
        data: {
          data: { goals },
        },
      } = await GoalService.getGoals(cookies.token, 1);
      setGoals(goals);
      setTask(null)
      setGoal(null)
      setShowLoader(false)
    } catch (error) {
      displayError(error);
      setShowLoader(false);
    }
  };

  useEffect(() => {
    if (cookies.user) {
      fetchGoals();
      setUser(cookies.user);
    }
  }, [cookies.token, cookies.user]);

  const router = createBrowserRouter([
    {
      path: "/",
      element: <Home />,
      errorElement: <NotFound backToHome={true} image="bad-request.jpg" heading="Page Not Found" />,
    },
    {
      path: "/login",
      element: <Login />,
    },
    {
      path: "/signup",
      element: <Signup />,
    }
  ]);
  return (
    <>
      {showLoader && <Backdrop show={true} />}
      {showLoader && <RotatingTriangles
          visible={true}
          height="80"
          width="80"
          color="#4fa94d"
          ariaLabel="rotating-triangles-loading"
          wrapperStyle={{}}
          wrapperClass="loader"
        />}
      <ToastContainer />
      <Navbar />
      <RouterProvider router={router} />
    </>
  );
}

export default App;
